var Vue;
var GameState;
(function (GameState) {
    GameState[GameState["Initial"] = "initial"] = "Initial";
    GameState[GameState["PreGame"] = "pre"] = "PreGame";
    GameState[GameState["Play"] = "play"] = "Play";
    GameState[GameState["PostGameDraw"] = "postDraw"] = "PostGameDraw";
    GameState[GameState["PostGamePlayer"] = "postPlayer"] = "PostGamePlayer";
    GameState[GameState["PostGameOpponent"] = "postOpponent"] = "PostGameOpponent";
    GameState[GameState["PostGameLose"] = "postLose"] = "PostGameLose";
})(GameState || (GameState = {}));
var Hand;
(function (Hand) {
    Hand[Hand["Rock"] = "r"] = "Rock";
    Hand[Hand["Paper"] = "p"] = "Paper";
    Hand[Hand["Scissors"] = "s"] = "Scissors";
})(Hand || (Hand = {}));
var Timer = (function () {
    function Timer(text, timeInMilliSeconds) {
        this.startTime = new Date().getTime();
        this.timeoutInMilliseconds = timeInMilliSeconds;
        this.value = 1;
        this.text = text;
    }
    Timer.prototype.IsRunning = function () {
        var now = new Date().getTime();
        var elapsed = now - this.startTime;
        if (elapsed > this.timeoutInMilliseconds) {
            this.value = 0;
            return false;
        }
        else {
            this.value = 1.0 - (elapsed / this.timeoutInMilliseconds);
            return true;
        }
    };
    return Timer;
})();
var timeLimitBothInMilliseconds = 500;
var timeLimitGakiInMilliseconds = 1000;
var App = (function () {
    function App() {
        this.right = true;
        this.messages = new Array();
        this.messageCount = this.messages.length;
        this.timer = null;
        this.gameState = GameState.Initial;
        this.opponentHand = null;
        this.opponentTimeout = false;
        this.playerHand = null;
        this.playerTimeout = false;
        this.gakiVersion = false;
        this.opponentPoints = 0;
        this.playerPoints = 0;
    }
    return App;
})();
new Vue({
    el: '#rpsApp',
    data: function () {
        return new App();
    },
    methods: {
        startTraditional: function () {
            this.startGame(false);
        },
        startGaki: function () {
            this.startGame(true);
        },
        startGame: function (gakiVersion) {
            this.gakiVersion = gakiVersion;
            this.gameState = GameState.Initial;
            this.opponentHand = null;
            this.opponentTimeout = false;
            this.playerHand = null;
            this.playerTimeout = false;
            this.timer = new Timer('Ready!', 1000);
            this.intervalId = setInterval(function (app) {
                if (app.timer.IsRunning()) {
                }
                else {
                    clearInterval(app.intervalId);
                    app.gameState = GameState.PreGame;
                    app.timer = new Timer('3', timeLimitBothInMilliseconds);
                    app.intervalId = setInterval(function (app) {
                        if (app.timer.IsRunning()) {
                        }
                        else {
                            clearInterval(app.intervalId);
                            app.timer = new Timer('2', timeLimitBothInMilliseconds);
                            app.intervalId = setInterval(function (app) {
                                if (app.timer.IsRunning()) {
                                }
                                else {
                                    clearInterval(app.intervalId);
                                    app.timer = new Timer('1', timeLimitBothInMilliseconds);
                                    app.intervalId = setInterval(function (app) {
                                        if (app.timer.IsRunning()) {
                                        }
                                        else {
                                            clearInterval(app.intervalId);
                                            if (gakiVersion) {
                                                app.playGaki();
                                            }
                                            else {
                                                app.playTraditional();
                                            }
                                        }
                                    }, 100, app);
                                }
                            }, 100, app);
                        }
                    }, 100, app);
                }
            }, 100, this);
        },
        playTraditional: function () {
            this.gameState = GameState.Play;
            this.timer = new Timer('Play!', timeLimitBothInMilliseconds);
            this.intervalId = setInterval(function (app) {
                if (app.timer.IsRunning()) {
                    app.drawOpponentHand(100 / timeLimitBothInMilliseconds);
                }
                else {
                    clearInterval(app.intervalId);
                    app.playerTimeout = !app.playerHand;
                    app.opponentTimeout = !app.opponentHand;
                    if (app.playerHand && app.opponentHand) {
                        app.gameState = app.getResultForTraditional(app.playerHand, app.opponentHand);
                    }
                    else if (app.playerHand) {
                        app.gameState = GameState.PostGamePlayer;
                    }
                    else if (app.opponentHand) {
                        app.gameState = GameState.PostGameOpponent;
                    }
                    else {
                        app.gameState = GameState.PostGameLose;
                    }
                    app.setPointsAndGloat();
                }
            }, 100, this);
        },
        playGaki: function () {
            this.gameState = GameState.Play;
            this.drawOpponentHand(1);
            this.timer = new Timer('Play!', timeLimitGakiInMilliseconds);
            this.intervalId = setInterval(function (app) {
                if (app.timer.IsRunning()) {
                }
                else {
                    clearInterval(app.intervalId);
                    app.playerTimeout = !app.playerHand;
                    app.opponentTimeout = !app.opponentHand;
                    if (app.playerHand && app.opponentHand) {
                        app.gameState = app.getResultForGaki(app.playerHand, app.opponentHand);
                    }
                    else if (app.playerHand) {
                        app.gameState = GameState.PostGamePlayer;
                    }
                    else if (app.opponentHand) {
                        app.gameState = GameState.PostGameOpponent;
                    }
                    else {
                        app.gameState = GameState.PostGameLose;
                    }
                    app.setPointsAndGloat();
                }
            }, 100, this);
        },
        drawOpponentHand: function (fraction) {
            //fraction means how many times opponent can draw hands. We add some randomnes when opponent draws hand.
            // He might even change hand sometimes or not draw at all in allowed time. Average should be that opponent draws once.
            var random = Math.random();
            if (random < fraction * 1.5) {
                var hand = Math.floor(Math.random() * 3);
                if (hand == 0) {
                    this.opponentHand = Hand.Rock;
                }
                else if (hand == 1) {
                    this.opponentHand = Hand.Paper;
                }
                else {
                    this.opponentHand = Hand.Scissors;
                }
            }
        },
        onScroll: function (info) {
            if (this.messages.length != this.messageCount) {
                info.ref.setScrollPosition(info.verticalSize, 700);
                this.messageCount = this.messages.length;
            }
        },
        getResultForTraditional: function (playerHand, opponentHand) {
            if (playerHand == opponentHand) {
                return GameState.PostGameDraw;
            }
            if (playerHand == Hand.Rock) {
                return opponentHand == Hand.Paper ? GameState.PostGameOpponent : GameState.PostGamePlayer;
            }
            if (playerHand == Hand.Paper) {
                return opponentHand == Hand.Scissors ? GameState.PostGameOpponent : GameState.PostGamePlayer;
            }
            if (playerHand == Hand.Scissors) {
                return opponentHand == Hand.Rock ? GameState.PostGameOpponent : GameState.PostGamePlayer;
            }
        },
        getResultForGaki: function (playerHand, opponentHand) {
            if (playerHand == opponentHand) {
                return GameState.PostGameOpponent;
            }
            if (playerHand == Hand.Rock) {
                return opponentHand == Hand.Paper ? GameState.PostGamePlayer : GameState.PostGameOpponent;
            }
            if (playerHand == Hand.Paper) {
                return opponentHand == Hand.Scissors ? GameState.PostGamePlayer : GameState.PostGameOpponent;
            }
            if (playerHand == Hand.Scissors) {
                return opponentHand == Hand.Rock ? GameState.PostGamePlayer : GameState.PostGameOpponent;
            }
        },
        setPointsAndGloat: function () {
            var message = null;
            if (this.gameState == GameState.PostGameOpponent) {
                this.opponentPoints++;
                message = "Thanks, the score is " + this.playerPoints + "-" + this.opponentPoints + ".";
            }
            if (this.gameState == GameState.PostGamePlayer) {
                this.playerPoints++;
                message = "Take it. The score is " + this.playerPoints + "-" + this.opponentPoints + ".";
            }
            if (message) {
                if (this.opponentPoints > this.playerPoints) {
                    message += ' I am winning.';
                }
                if (this.opponentPoints < this.playerPoints) {
                    message += ' You are leading.';
                }
                this.messages.push(message);
            }
        }
    },
    created: function () {
        this.messages.push('Welcome to Rock Paper Scissors! Please, start a new game from bottom right. You can play two versions of game; traditional and Gaki no Tsukai.');
        this.messages.push('<u>R</u>ock wins scissors. <u>S</u>cissors wins paper. <u>P</u>aper wins rock.');
        this.messages.push('In traditional version you and your opponent show hands at the same time - winning hand wins the game and same hands is a draw.');
        this.messages.push('In Gaki version your opponent shows hand first and then you have to show <strong>losing</strong> hand to win game - otherwise you lose.');
    }
});
